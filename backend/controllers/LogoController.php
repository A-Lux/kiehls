<?php

namespace backend\controllers;

use backend\models\FileUpload;
use Yii;
use common\models\Logo;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * LogoController implements the CRUD actions for Logo model.
 */
class LogoController extends BackendController
{
    private $myID = 1;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Logo models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('view', [
            'model' => $this->findModel($this->myID),
        ]);
    }



    /**
     * Updates an existing Logo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $model = $this->findModel($this->myID);
        $current_logo = $model->image;
        $current_favicon = $model->favicon;

        if ($model->load(Yii::$app->request->post())) {

            $upload = new FileUpload();

            $logo = UploadedFile::getInstance($model, 'image');
            $model->image = $upload->uploadFile($logo, $model->folder, $current_logo);

            $favicon = UploadedFile::getInstance($model, 'favicon');
            $model->favicon = $upload->uploadFile($favicon, $model->folder, $current_favicon);

            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }



    /**
     * Finds the Logo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Logo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Logo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
