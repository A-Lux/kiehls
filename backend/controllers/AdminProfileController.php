<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.08.2019
 * Time: 15:52
 */

namespace backend\controllers;
use common\models\User;
use Yii;
use yii\web\Controller;

class AdminProfileController extends BackendController
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex(){
        $admin = User::getAdmin();
        return $this->render('index',compact('admin'));
    }

    public function actionSetProfile($username, $password){
        $admin = User::getAdmin();
        $admin->username = $username;
        $admin->setPassword($password);
        if($admin->save(false)){
            Yii::$app->session->setFlash('profile_success','Профиль успешно обновлен!');
            return $this->redirect('/admin/admin-profile');
        }else{
            Yii::$app->session->setFlash('profile_error','Что-то пошло не так!');
            return $this->redirect('/admin/admin-profile');
        }
    }

}
