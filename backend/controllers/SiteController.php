<?php
namespace backend\controllers;

use common\models\User;
use tests\app\controllers\sub\ActionController;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{


    public  $layout = "auth";


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect('/admin/admin-profile');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }else{
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            } else {
                $model->password = '';

                return $this->render('login', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }



    public function actionUpdatePassword(){
        $admin = User::find()->where('role=10')->one();
        $code = $admin->getRandomPassword();
        if(isset($_SESSION['confirmCode'])){
            return $this->render('confirm-code');
        }else{
            $_SESSION['confirmCode'] = $code;
            if($this->sendNewPassword($admin->username,$code)){
                return $this->render('confirm-code');
            }
        }

    }

    public function actionConfirmCode($code){
        if($_SESSION['confirmCode'] == $code){
            return $this->render('update-password',compact('code'));
        }else{
            Yii::$app->session->setFlash('code_error','неверный код!');
            return $this->render('confirm-code');
        }
    }

    public function actionNewPassword($code, $password, $confirmPassword){
        if($_SESSION['confirmCode'] == $code) {
            if ($password == $confirmPassword) {
                $admin = User::getAdmin();
                $admin->setPassword($password);
                if ($admin->save(false)) {
                    unset($_SESSION['confirmCode']);
                    Yii::$app->session->setFlash('set_password_success', 'Пароль успешно обновлен!');
                    return $this->redirect('login');
                } else {
                    Yii::$app->session->setFlash('set_password_error', 'Упс, что-то пошло не так!');
                    return $this->redirect('/admin/site/confirm-code?code='.$code);
                }

            } else {
                Yii::$app->session->setFlash('set_password_error', 'Пароли не совпадает!');
                return $this->redirect('/admin/site/confirm-code?code='.$code);
            }
        }else{
            return $this->render('confirm-code');
        }
    }

    private function sendNewPassword($email, $code) {

        $emailSend = Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($email)
            ->setSubject('Кто-то пытается изменить ваш пароль!')
            ->setHtmlBody("<p> Код для изменение пароля : $code</p>");
        return $emailSend->send();
    }
}
