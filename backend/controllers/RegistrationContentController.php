<?php

namespace backend\controllers;

use backend\models\FileUpload;
use Yii;
use common\models\RegistrationContent;
use backend\models\search\RegistrationContentSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * RegistrationContentController implements the CRUD actions for RegistrationContent model.
 */
class RegistrationContentController extends  BackendController
{

    private $myID = 1;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RegistrationContent models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('view', [
            'model' => $this->findModel($this->myID),
        ]);
    }



    /**
     * Updates an existing RegistrationContent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $model = $this->findModel($this->myID);
        $current_image = $model->rectangle_image;

        if ($model->load(Yii::$app->request->post())) {

            $upload = new FileUpload();
            $image = UploadedFile::getInstance($model, 'rectangle_image');
            $model->rectangle_image = $upload->uploadFile($image, $model->folder, $current_image);

            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Finds the RegistrationContent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RegistrationContent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RegistrationContent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
