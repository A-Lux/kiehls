<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserQuestionnaire */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Анкеты пользователей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-questionnaire-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Удалить', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
//                'method' => 'post',
//            ],
//        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return $model->userFio;
                },
            ],
            [
                'attribute' => 'questionnaire_id',
                'value' => function($model){
                    return $model->questionnaireContent;
                },
            ],
            [
                'format' => 'raw',
                'attribute' => 'answer',
                'value' => function($data){
                    return $data->answer;
                }
            ],
        ],
    ]) ?>

</div>
