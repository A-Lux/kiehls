<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserQuestionnaireSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Анкеты пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-questionnaire-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'filter' => \common\models\User::getList(),
                'value' => function($model){
                    return $model->userFio;
                },
            ],
            [
                'attribute' => 'questionnaire_id',
                'filter' => \common\models\Questionnaire::getList(),
                'value' => function($model){
                    return $model->questionnaireContent;
                },
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}'],
        ],
    ]); ?>


</div>
