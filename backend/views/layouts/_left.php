<aside class="main-sidebar">

    <div class="user-panel">
        <div class="pull-left image">
            <img src="/admin/images/user2-160x160.jpg" class="img-circle" alt="Вы">
        </div>
        <div class="pull-left info">
            <p>Alexander Pierce</p>
            <a href="#"><i class="fa fa-circle text-success"></i> В сети</a>
        </div>
    </div>

    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ОСНОВНАЯ НАВИГАЦИЯ</li>
    </ul>
    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [

                    ['label' => 'Пользователей', 'icon' => 'fa fa-user', 'url' => ['/users'],'active' => $this->context->id == 'users'],
                    ['label' => 'Новостная рассылка', 'icon' => 'fa fa-user', 'url' => ['/newsletter'],'active' => $this->context->id == 'newsletter'],
                    ['label' => 'Анкеты пользователей', 'icon' => 'fa fa-user', 'url' => ['/user-questionnaire'], 'active' => $this->context->id == 'user-questionnaire'],
                ],
            ]
        ) ?>

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">ТЕКСТОВАЯ ИНФОРМАЦИЯ</li>
        </ul>


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [

                    ['label' => 'Тексты', 'icon' => 'fa', 'url' => ['/source-message/']],
                    ['label' => 'Страницы', 'icon' => 'fa fa-user', 'url' => ['/pages'],'active' => $this->context->id == 'pages'],
                    [
                        'label' => 'Главная страница',
                        'icon' => 'fa fa-home',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Контент', 'icon' => 'fa fa-user', 'url' => ['/main-content'], 'active' => $this->context->id == 'main-content'],
                            ['label' => 'Процессы', 'icon' => 'fa fa-user', 'url' => ['/main-process'], 'active' => $this->context->id == 'main-process'],
                        ],
                    ],

                    [
                        'label' => 'Регистрация',
                        'icon' => 'fa fa-home',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Регистрация', 'icon' => 'fa fa-user', 'url' => ['/registration-content'], 'active' => $this->context->id == 'registration-content'],
                            ['label' => 'Согласие', 'icon' => 'fa fa-user', 'url' => ['/agreement'], 'active' => $this->context->id == 'agreement'],
                        ],
                    ],
                    [
                        'label' => 'Анкетирование',
                        'icon' => 'fa fa-home',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Вопросы', 'icon' => 'fa fa-user', 'url' => ['/questionnaire'], 'active' => $this->context->id == 'questionnaire'],
                            ['label' => 'Варианты ответа', 'icon' => 'fa fa-user', 'url' => ['/answer-option'], 'active' => $this->context->id == 'answer-option'],
                        ],
                    ],
                    ['label' => 'КЛУБ ДРУЗЕЙ KIEHL\'S', 'icon' => 'fa fa-user', 'url' => ['/faq'],'active' => $this->context->id == 'faq'],
                    ['label' => 'Лого и Favicon', 'icon' => 'fa fa-user', 'url' => ['/logo'],'active' => $this->context->id == 'logo'],
                    ['label' => 'Сообщения', 'icon' => 'fa fa-user', 'url' => ['/message'],'active' => $this->context->id == 'message'],
                    ['label' => 'Страница 404', 'icon' => 'fa fa-user', 'url' => ['/error-page'],'active' => $this->context->id == 'error-page'],
                ],
            ]
        ) ?>

    </section>

</aside>
