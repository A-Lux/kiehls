<?php  
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini"></span><span class="logo-lg">Админ. панель</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
<!--                <li class="dropdown messages-menu">-->
<!--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"  title="Вопросы">-->
<!--                        <i class="fa fa-envelope-o"></i>-->
<!--                        <span class="label --><?//=count((array)Yii::$app->view->params['feedback'])==0?'label-default':'label-success';?><!--">-->
<!--                            --><?//=count((array)Yii::$app->view->params['feedback']);?><!--</span>-->
<!--                    </a>-->
<!--                    <ul class="dropdown-menu">-->
<!--                        <li class="header">У вас --><?//=count((array)Yii::$app->view->params['feedback']);?><!-- непрочитанных вопросы</li>-->
<!--                        <li>-->
<!---->
<!--                            <ul class="menu">-->
<!--                                --><?// if(Yii::$app->view->params['feedback'] != null):?>
<!--                                    --><?// foreach (Yii::$app->view->params['feedback'] as $v):?>
<!--                                        <li>-->
<!--                                            <a href="/admin/feedback/view?id=--><?//=$v->id;?><!--">-->
<!--                                                <i class="fa fa-user text-green"></i> --><?//=$v->name;?>
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                    --><?// endforeach;?>
<!--                                --><?// endif;?>
<!--                            </ul>-->
<!--                        </li>-->
<!--                        <li class="footer"><a href="/admin/feedback/">Посмотреть все вопросы</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Админ">
                        <!-- The user image in the navbar-->
                        <img src="/admin/images/user2-160x160.jpg" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">Alexander Pierce</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="/admin/images/user2-160x160.jpg" class="img-circle" alt="User Image">

                            <p>
                                Alexander Pierce - Administrator
                                <small>2017-2019</small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/admin/admin-profile" class="btn btn-default btn-flat">Профиль</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Выйти',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
