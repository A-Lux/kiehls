<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="source-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 5, 'disabled' => 'disabled']) ?>

    <? foreach($message as $v){ ?>
        <div class="form-group field-sourcemessage-category">
            <label class="control-label" for="sourcemessage-category" style="text-transform: uppercase"><?=$v->language?></label>
            <textarea type="text" id="sourcemessage-category" class="form-control" name="<?='message_'.$v->language?>"><?=$v->translation;?></textarea>
            <div class="help-block"></div>
        </div>
    <? } ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
