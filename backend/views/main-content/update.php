<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MainContent */

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => 'Контент', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="main-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
