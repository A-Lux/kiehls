<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MainContent */

$this->title = 'Контент';
$this->params['breadcrumbs'][] = ['label' => $this->title ];
\yii\web\YiiAsset::register($this);
?>
<div class="main-content-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'subtitle',
            'description:ntext',
            'red_text:ntext',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
