<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RegistrationContent */

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="registration-content-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'rectangle_title',

            [
                'format' => 'html',
                'attribute' => 'rectangle_image',
                'filter' => '',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width' => 300]);
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'rectangle_desc',
                'value' => function($data){
                    return $data->rectangle_desc;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'footer_content',
                'value' => function($data){
                    return $data->footer_content;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'f_agreement',
                'value' => function($data){
                    return $data->f_agreement;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 's_agreement',
                'value' => function($data){
                    return $data->s_agreement;
                }
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
