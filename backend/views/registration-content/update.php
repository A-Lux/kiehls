<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RegistrationContent */

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => 'Регистрация', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="registration-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
