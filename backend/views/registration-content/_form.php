<?php

use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RegistrationContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="registration-content-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rectangle_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rectangle_image')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload' => false ,
        ] ,
        'options' => ['accept' => 'image/*'],
    ]);
    ?>

    <?= $form->field($model, 'rectangle_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'footer_content' )->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
            'preset' => 'standard',    // basic, standard, full
            'inline' => false,      //по умолчанию false
        ])
    ]); ?>

    <?= $form->field($model, 'f_agreement' )->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
            'preset' => 'standard',    // basic, standard, full
            'inline' => false,      //по умолчанию false
        ])
    ]); ?>

    <?= $form->field($model, 's_agreement' )->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
            'preset' => 'standard',    // basic, standard, full
            'inline' => false,      //по умолчанию false
        ])
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
