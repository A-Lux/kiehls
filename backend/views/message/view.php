<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->subject;
$this->params['breadcrumbs'][] = ['label' => 'Сообщения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'subject',
                'value' => function ($model) {
                    return $model->subject;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'message',
                'value' => function ($model) {
                    return $model->message;
                },
                'format' => 'raw',
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
