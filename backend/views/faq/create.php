<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ErrorPage */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'КЛУБ ДРУЗЕЙ KIEHL\'S', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="error-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
