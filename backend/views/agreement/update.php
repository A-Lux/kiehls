<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Согласие', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="agreement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
