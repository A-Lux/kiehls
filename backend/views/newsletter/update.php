<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Newsletter */

$this->title = 'Редактирование новостной рассылки: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Новостная рассылка', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="newsletter-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
