<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Newsletter */

$this->title = 'Создание новостной рассылки';
$this->params['breadcrumbs'][] = ['label' => 'Новостная рассылка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
