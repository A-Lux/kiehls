<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\NewsletterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новостная рассылка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => ' {view} {update} {delete} {send-me}  {send-all} ',
                'buttons'=>[
                    'send-all' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-envelope"></span>', '/admin/newsletter/send-all?id='.$model->id, [
                            'title' => Yii::t('yii', 'Отправить всем '),
                        ]);
                    },

                     'send-me' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-question-sign"></span>', '/admin/newsletter/send-me?id='.$model->id, [
                            'title' => Yii::t('yii', 'Отправить мне '),
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
