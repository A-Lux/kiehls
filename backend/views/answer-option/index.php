<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AnswerOptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Варианты ответа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answer-option-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'questionnaire_id',
                'filter' => \common\models\Questionnaire::getList(),
                'value' => function($model){
                    return $model->questionnaireContent;
                },
            ],

            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($data){
                    return $data->content;
                }
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
