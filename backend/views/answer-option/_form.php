<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AnswerOption */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="answer-option-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'questionnaire_id')->dropDownList(\common\models\Questionnaire::getList()) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
