<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AnswerOption */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Варианты ответа', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answer-option-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
