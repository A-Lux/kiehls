<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Logo */

$this->title = 'Лого и Favicon';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="logo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getLogo(), ['width' => 200]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'favicon',
                'value' => function($data){
                    return Html::img($data->getFavicon(), ['width' => 100]);
                }
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
