<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Logo */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Лого и Favicon', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="logo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
