<?php

namespace backend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RegistrationContent;

/**
 * RegistrationContentSearch represents the model behind the search form of `common\models\RegistrationContent`.
 */
class RegistrationContentSearch extends RegistrationContent
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'rectangle_title', 'rectangle_image', 'rectangle_desc', 'footer_content', 'f_agreement', 's_agreement', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RegistrationContent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'rectangle_title', $this->rectangle_title])
            ->andFilterWhere(['like', 'rectangle_image', $this->rectangle_image])
            ->andFilterWhere(['like', 'rectangle_desc', $this->rectangle_desc])
            ->andFilterWhere(['like', 'footer_content', $this->footer_content])
            ->andFilterWhere(['like', 'f_agreement', $this->f_agreement])
            ->andFilterWhere(['like', 's_agreement', $this->s_agreement]);

        return $dataProvider;
    }
}
