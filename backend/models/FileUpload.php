<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class FileUpload extends Model{

    public $image;

    public function uploadFile(UploadedFile $file = null, $folder, $currentFile = "")
    {
        if($file != null){
            $this->deleteCurrentImage($currentFile, $folder);
            $this->image = $file;
            $filename = $this->generateFilename();
            $this->saveImageToFolder($folder . $filename);
            return $filename;
        }else{
            return $currentFile;
        }
    }

    private function saveImageToFolder($link){
        $this->image->saveAs($link);
    }

    private function generateFilename()
    {
        return strtolower(md5(uniqid($this->image->baseName)) . '.' . $this->image->extension);
    }

    public function deleteCurrentImage($currentFile, $folder)
    {
        if($this->fileExists($currentFile, $folder))
        {
            unlink($folder . $currentFile);
        }
    }

    public function fileExists($currentFile, $folder)
    {
        if(!empty($currentFile) && $currentFile != null)
        {
            return file_exists($folder. $currentFile);
        }
    }


}
