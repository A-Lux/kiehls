<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'uww.asia',
                'username' => 'info@uww.asia',
                'password' => 'Y6&8i1xt',
                'port' => '25',//465
            ],
            'useFileTransport' => false,
        ],

        'formatter' => [
            'language' => 'ru-RU',
            'timeZone' => 'Asia/Almaty',
            'dateFormat' => 'dd MMMM',
            'datetimeFormat' => 'php:d-m-Y',
            'timeFormat' => 'php:h:i:s A',
            'decimalSeparator' => '.',
            'thousandSeparator' => '',
            'class' => 'yii\i18n\Formatter',
        ],

        'i18n' => [
            'translations' =>[
                '*' => [
                    'class' => yii\i18n\DbMessageSource::className(),
                    'sourceLanguage' => 'ru-RU',
                    'sourceMessageTable'    => '{{%source_message}}',
                    'messageTable'          => '{{%message}}',
                ],
            ],
        ],

    ],
];
