<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "answer_options".
 *
 * @property int $id
 * @property int $questionnaire_id
 * @property string $content
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Questionnaire $questionnaire
 */
class AnswerOption extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answer_options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['questionnaire_id', 'content'], 'required'],
            [['questionnaire_id'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['questionnaire_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questionnaire::className(), 'targetAttribute' => ['questionnaire_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questionnaire_id' => 'Вопрос',
            'content' => 'Вариант ответа',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public function getQuestionnaire()
    {
        return $this->hasOne(Questionnaire::className(), ['id' => 'questionnaire_id']);
    }

    public function getQuestionnaireContent()
    {
        return $this->questionnaire ? $this->questionnaire->content : '';
    }


}
