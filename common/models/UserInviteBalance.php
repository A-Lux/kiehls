<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_invite_balances".
 *
 * @property int $id
 * @property int $user_id
 * @property int $balance
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property User $user
 */
class UserInviteBalance extends \yii\db\ActiveRecord
{

    const user_invite_balance = 100;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_invite_balances';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'balance'], 'required'],
            [['user_id', 'balance'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'balance' => 'Баланс',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function createNew($user_id)
    {
        $this->user_id = $user_id;
        $this->balance = 0;
        return $this->save();
    }

    public static function findByUser(){
        return self::findOne(['user_id' => Yii::$app->user->id]);
    }

}
