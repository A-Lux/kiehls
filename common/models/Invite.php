<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "invites".
 *
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property User $user
 */
class Invite extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'token'], 'required'],
            [['user_id'], 'integer'],
            [['token'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'token' => 'Токен',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function createNew(){

        $this->user_id = Yii::$app->user->id;
        $this->generateToken();
        return $this->save();
    }

    public function generateToken()
    {
        $this->token = Yii::$app->security->generateRandomString();
    }

    public static function findByToken($token){
        return self::findOne(['token' => $token]);
    }


    public static function findByUser(){
        return self::findOne(['user_id' => Yii::$app->user->id]);
    }
}
