<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_questionnaires".
 *
 * @property int $id
 * @property int $user_id
 * @property int $questionnaire_id
 * @property string $answer
 *
 * @property Questionnaire $questionnaire
 * @property User $user
 */
class UserQuestionnaire extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_questionnaires';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'questionnaire_id', 'answer'], 'required'],
            [['user_id', 'questionnaire_id'], 'integer'],
            [['answer'], 'string'],
            [['questionnaire_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questionnaire::className(), 'targetAttribute' => ['questionnaire_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'questionnaire_id' => 'Вопрос',
            'answer' => 'Ответ',
        ];
    }

    /**
     * Gets query for [[Questionnaire]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionnaire()
    {
        return $this->hasOne(Questionnaire::className(), ['id' => 'questionnaire_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public function getQuestionnaireContent()
    {
        return $this->questionnaire ? $this->questionnaire->content : '';
    }


    public function getUserFio()
    {
        return $this->user ? $this->user->getFio() : '';
    }

    public function createNew($questionnaire_id, $answer){
        $this->user_id = Yii::$app->user->getId();
        $this->questionnaire_id = $questionnaire_id;
        $this->answer = $answer;
        return $this->save();
    }
}
