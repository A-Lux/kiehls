<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "questionnaires".
 *
 * @property int $id
 * @property string $content
 * @property int $status
 * @property int $another_variant_status
 * @property int $sort
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Questionnaire extends \yii\db\ActiveRecord
{

    const is_active = 1;
    const is_closed = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questionnaires';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'status', 'another_variant_status'], 'required'],
            [['content'], 'string'],
            [['status', 'sort', 'another_variant_status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Вопрос',
            'status' => 'Статус',
            'another_variant_status' => 'Другой вариант',
            'sort' => 'Сортировка',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public function setSort(){
        $this->sort = $this->findLast() ? $this->findLast()->sort + 1 : 1;
    }

    public function findLast(){
        return self::find()->orderBy('id DESC')->one();
    }

    public static function getList(){
        return ArrayHelper::map(self::getAll(), 'id', 'content');
    }

    public static function getAll(){
        return self::find()->all();
    }

    public static function getActiveQuestions(){
        return self::findAll(['status' => self::is_active]);
    }

    public function getAnswerOptions(){
        return $this->hasMany(AnswerOption::className(), ['questionnaire_id' => 'id']);
    }
}
