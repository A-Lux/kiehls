<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "Message".
 *
 * @property int $id
 * @property string $subject
 * @property string $message
 * @property string $created_at
 * @property string $updated_at
*/

class Message extends \yii\db\ActiveRecord
{

    const register = 1;
    const forget_password = 2;


    public static function tableName()
    {
        return 'messages';
    }

    public function rules()
    {
        return [
            [['subject', 'message'], 'required'],
            [['subject', 'message'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Тема',
            'message' => 'Сообщение',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public static function getRegisterMessage(){
        return self::findOne(self::register);
    }


    public static function getForgetPasswordMessage(){
        return self::findOne(self::forget_password);
    }


}
