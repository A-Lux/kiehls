<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "agreement".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Agreement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agreement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Содержание',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public static function getContent(){
        return self::find()->select('title, content')->one();
    }
}
