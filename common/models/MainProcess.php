<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "main_processes".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class MainProcess extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'main_processes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'content' => 'Описание',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public static function getAll(){
        return self::find()->select('id, title, content')->all();
    }
}
