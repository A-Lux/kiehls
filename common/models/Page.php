<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keyword
 * @property string|null $url
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meta_title'], 'required'],
            [['meta_description', 'meta_keyword'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['meta_title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_title' => 'Мета заголовок',
            'meta_description' => 'Мета Описание',
            'meta_keyword' => 'Ключевые слова',
            'url' => 'Ссылка',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public static function findByUrl($url){
        return self::findOne(['url' => $url]);
    }
}
