<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_profiles".
 *
 * @property int $id
 * @property string $fio
 * @property string $telephone
 * @property int $phone_confirm
 * @property string $date_of_birth
 * @property string $avatar
 * @property int $user_id
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class UserProfile extends \yii\db\ActiveRecord
{

    public $folder = 'img/account/';

    const is_confirmed = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'user_id'], 'required'],
            [['date_of_birth', 'created_at', 'updated_at'], 'safe'],
            [['user_id', 'phone_confirm'], 'integer'],
            [['fio', 'telephone'], 'string', 'max' => 255],
            [['avatar'],'file','extensions' => 'png,jpg,jpeg', 'maxSize' => 1024 * 1024 * 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'telephone' => 'Телефон',
            'date_of_birth' => 'Дата рождения',
            'user_id' => 'User ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public function getBirthdayDate(){
        return Yii::$app->formatter->asDate($this->date_of_birth, 'long');
    }

    public function confirmPhone(){
         $this->phone_confirm = self::is_confirmed;
         return $this->save(false);
    }

    public function updateTelephone($telephone){
        $this->telephone = $telephone;
        return $this->save(false);
    }

    public function getAvatar(){
        return $this->avatar ? '/'.$this->folder.''.$this->avatar : '/img/avatar.png';
    }


    public static function findByUser(){
        return self::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->one();
    }

}
