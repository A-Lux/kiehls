<?php

/** @var array $params */

return [
        'class' => 'yii\web\UrlManager',
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'cache' => false,
        'rules' => [
            'sign-up' => 'site/sign-up',
            'sign-in' => 'site/sign-in',
            'forgot-password' => 'site/forgot-password'
    ],
];
