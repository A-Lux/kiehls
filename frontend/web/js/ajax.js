function showError(error){
    let modal = $('#error');
    let modal_text = $('#error_text p');
    modal_text.html(error);
    modal.show();
}


jQuery(function($){
    $('#registration_phone').mask("+7 (999) 999-99-99");
});

$(document).ready(function() {

    $("#btnTelConfirm").click(function (e) {
        e.preventDefault();
        $.ajax({
            url: "/account/confirm-tel",
            data: $(this).closest("form").serialize(),
            type: "POST",
            success: function (data) {

                $('#confirmTelModal').hide();
                if (data == 1) {
                    window.location.href = "/account/credit";
                } else if(data == 0) {
                   showError('Неверный код.')
                }else{
                    $('#confirm_code_error').html(data);
                }

            },
            error: function () {
                Swal.fire('', 'Внутренняя ошибка сервера.', 'error');
            },
        });
    });


    $("#signUpForm").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/sign-up",
            data: $(this).closest("form").serialize(),
            type: "POST",
            success: function (data) {
                if (data == 1) {
                    window.location.href = "/account";
                }else if(data == 0){

                }else{
                    showError(data);
                }
            },
            error: function () {
                Swal.fire('', 'Внутренняя ошибка сервера.', 'error');
            },
        });
    });


    $("#updateProfileForm").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/account/index",
            data: $(this).closest("form").serialize(),
            type: "POST",
            success: function (data) {
                if (data == 1) {
                    $('.fancybox-close')[0].click();
                    $('#account_error').hide();
                    Swal.fire('Ваши изменения', 'успешно сохранены', 'success');

                    setTimeout(function() {
                        window.location.href = "/account";
                    }, 3000);

                }else if(data == 0){

                }else{
                    $('.fancybox-close')[0].click();
                    $('#account_error_text').html(data);
                    $('#account_error').show();
                }
            },
            error: function () {
                Swal.fire('', 'Внутренняя ошибка сервера.', 'error');
            },
        });
    });




    $('body').on('change', '#uploadMyAvatarInput',function(e){

        var file = this.files[0];
        var form_data = new FormData();
        form_data.append('file', file);

        $.ajax({
            url: '/account/upload-avatar',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "POST",
            success: function (response) {
                window.location.href = "/account";
            },
            error: function (response) {
                Swal.fire('', 'Внутренняя ошибка сервера.', 'error');
            },
        });
    });



    $('.questionnaire_another_option').on("click", function (e) {
        let question_id = $(this).attr('data-id');
        let another = $('#another_input_'+question_id);
        another.attr("required", true);
    });


    $('.questionnaire_option').on("click", function (e) {
        let question_id = $(this).attr('data-id');
        let another = $('#another_input_'+question_id);
        another.attr("required", false);
        another.val('');
    });

});