<?php


namespace frontend\controllers;


use backend\models\FileUpload;
use common\models\AnswerOption;
use common\models\Auth;
use common\models\Faq;
use common\models\Invite;
use common\models\Questionnaire;
use common\models\RegistrationContent;
use common\models\UserInviteBalance;
use common\models\UserProfile;
use common\models\UserQuestionnaire;
use frontend\models\AvatarUploadForm;
use frontend\models\TelephoneConfirmForm;
use frontend\models\TelephoneForm;
use frontend\models\UpdateProfileForm;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;


class AccountController extends FrontendController
{

   public function beforeAction($action)
   {
       if(Yii::$app->user->isGuest){
           return $this->redirect('/sign-in')->send();
       }else{
           Yii::$app->params['questionnaire'] = Questionnaire::getActiveQuestions();
           Yii::$app->params['questionnaire_status'] = $this->getQuestionnaireStatus();
       }

       return parent::beforeAction($action); // TODO: Change the autogenerated stub
   }


    public function actionIndex()
    {

        $profile = UserProfile::findByUser();
        $model = new UpdateProfileForm();
        $avatarUpload = new AvatarUploadForm();
        $auth = Auth::findByUser();

        if (Yii::$app->request->isAjax)
        {
            if ($model->load(Yii::$app->request->post())) {
                return $model->update();
            }
        }

        return $this->render('index', compact('profile', 'model', 'avatarUpload', 'auth'));
    }


    public function actionCredit()
    {
        $profile = UserProfile::findByUser();
        $content = RegistrationContent::getContent();
        $faq = Faq::getAll();

        $model = new TelephoneForm();
        $model->telephone = $profile->telephone;

        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post())) {
                $model->updateTel();
            }
        }

        return $this->render('credits', compact('profile', 'model', 'content', 'faq'));
    }



    public function actionInvite()
    {

        if(!($invite = Invite::findByUser())){
            $invite = new Invite();
            $invite->createNew();
        }

        $balance = 0;
        $title = "Приглашаю тебя в Клуб Друзей Kiehl's";
        $img = Yii::$app->request->hostInfo.'/img/invitation-social-image.jpg';
        $invite_url = Yii::$app->request->hostInfo.Url::to(['/site/invite', 'token' => $invite->token]);

        if($user_balance = UserInviteBalance::findByUser()){
            $balance = $user_balance->balance;
        }

        return $this->render('invite', compact('invite_url', 'title', 'img', 'balance'));
    }


    public function actionConfirmTel(){

        if (Yii::$app->request->isAjax){
            $model = new TelephoneConfirmForm();
            if ($model->load(Yii::$app->request->post())) {
                return $model->confirm();
            }
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionDeleteSocialAuthentication($source){

        $auth = Auth::findByUser();
        if(isset($auth[$source])){
            $auth[$source]->delete();
        }

        return $this->redirect(['/account/index']);
    }


    public function actionAddSocialAuthentication($source){

        $auth = new Auth();
        $auth->createNew($source);

        return $this->redirect(['/account/index']);
    }



    public function actionUploadAvatar(){

        if (Yii::$app->request->isAjax){

            $avatar = $_FILES["file"];

            if ((($avatar["type"] == "image/jpeg") || ($avatar["type"] == "image/jpg")
                    || ($avatar["type"] == "image/png")) && ($avatar["size"] < 1024 * 1024 * 4)){

                if($profile = UserProfile::findOne(['user_id' => Yii::$app->user->id])){

                    $filename = time().'_'.$avatar['name'];
                    if(move_uploaded_file($avatar['tmp_name'], $profile->folder . $filename)){

                        $upload = new FileUpload();
                        if($upload->fileExists($profile->avatar, $profile->folder)){
                            unlink($profile->folder . $profile->avatar);
                        }

                        $profile->avatar = $filename;
                    }

                    if ($profile->save(false)) {
                        return 1;
                    }
                }
            }

        }else{
            throw new NotFoundHttpException();
        }

        return 0;
    }



    public function actionQuestionnaire(){

        if (Yii::$app->request->isPost){

            $data = Yii::$app->request->post();
            $questions = Questionnaire::getActiveQuestions();

            foreach ($questions as $question){

                if($question->another_variant_status && $data['another_'.$question->id] != null){
                    $answer = $data['another_'.$question->id];
                }else{
                    $answer_id = $data['q_'.$question->id];
                    $answer_option = AnswerOption::findOne($answer_id);
                    $answer = $answer_option->content;
                }

                $user_questionnaire = new UserQuestionnaire();
                $user_questionnaire->createNew($question->id, $answer);
            }

            Yii::$app->getSession()->setFlash('questionnaire_success_msg', Yii::t('questionnaire', "Спасибо, анкета успешно сохранен!"));

            return $this->redirect(['index']);

        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionDeleteAvatar(){

        if($profile = UserProfile::findOne(['user_id' => Yii::$app->user->id])){
            $avatar = $profile->avatar;
            $profile->avatar = null;

            if ($profile->save(false)) {
                if($avatar != null && file_exists($profile->folder . $avatar)){
                    unlink($profile->folder . $avatar);
                }
            }
        }

        return $this->redirect(['/account/index']);
    }



    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    private function getQuestionnaireStatus(){

        $status = false;
        if(Questionnaire::find()->exists()){
            if(!UserQuestionnaire::findOne(['user_id' => Yii::$app->user->getId()])){
                $status = true;
            }
        }

        return $status;
    }
}