<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.02.2020
 * Time: 12:21
 */

namespace frontend\models;

use common\models\UserProfile;
use Yii;
use yii\base\Model;

/**
 * This is the model class for tables "User, UserProfile".
 *
 * @property int $code
 */

class TelephoneForm extends RegistrationForm
{
    public $telephone;
    public $agreement;

    const success = 1;
    const failed = 0;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telephone'], 'required','message' => 'Обязательное поле'],
            [['telephone'], 'match', 'pattern' => '/^\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/', 'message' => 'Обязательное поле'],
            [['agreement'], 'required', 'message' => 'Обязательное поле'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'telephone' => 'Телефон',
        ];
    }


    public function updateTel(){

        if($this->validate()){

           $profile = UserProfile::findByUser();
           if($profile){
               if($profile->updateTelephone($this->telephone)){
                   $this->sendMessageToPhoneNumber();
                   return self::success;
               }

           }

            return self::failed;

        }else{

            $error = "";
            $errors = $this->getErrors();
            foreach ($errors as $v) {
                $error .= $v[0];break;
            }

            return $error;
        }

    }


}
