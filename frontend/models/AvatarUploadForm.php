<?php

namespace frontend\models;

use backend\models\FileUpload;
use common\models\User;
use common\models\UserProfile;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * @property string $avatar
 */

class AvatarUploadForm extends Model
{
    public $avatar;


    const success = 1;
    const failed = 0;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['avatar'],'file','extensions' => 'png,jpg,jpeg', 'maxSize' => 1024 * 1024 * 4],
        ];
    }

    public function attributeLabels()
    {
        return [
            'avatar' => 'Аватар',
        ];
    }


}
