<?php

namespace frontend\models;


use common\models\Subscription;
use common\models\User;
use common\models\UserProfile;
use yii\base\Model;


/**
 * This is the model class for tables "User, UserProfile".
 *
 * @property int $id
 * @property string $fio
 * @property string $telephone
 * @property string $email
 * @property string $password
 * @property string $password_confirm
 * @property int $birth_day
 * @property int $birth_month
 * @property int $birth_year
 * @property string $current_password
 * @property int $status_update_pass
 */


class UpdateProfileForm  extends Model
{
    public $fio;
    public $telephone;
    public $email;

    public $current_password;
    public $password;
    public $password_confirm;

    public $birth_day;
    public $birth_month;
    public $birth_year;

    public $status_update_pass;
    public $subscription;

    const success = 1;
    const failed = 0;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'telephone', 'email', 'current_password'], 'required'],
            [['email'], 'email'],
            [['birth_day', 'birth_month', 'birth_year'], 'number'],

            [['password','password_confirm'], 'string', 'min' => 6, 'max' => 255],
            [['password_confirm'], 'compare', 'compareAttribute' => 'password'],

            [['status_update_pass', 'subscription'], 'string'],

            [['fio'], 'match', 'pattern' => '/[А-Яа-яЁё]/u', 'message' => 'Смените раскладку'],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Имя Фамилия',
            'telephone' => 'Номер телефона',
            'email' => 'E-mail',
            'password' => 'Пароль',
            'password_confirm' => 'Подтвердите пароль',
            'current_password' => 'Текущий пароль',
            'status_update_pass' => 'Подписаться на рассылку Kiehl\'s (Email/SMS)'
        ];
    }


    public function update(){

        if($error = $this->validateForm()){
            return $error;
        }else{
            if ($this->updateUser()) {
                $this->addUserSubscribeList();
                return self::success;
            }
        }

        return self::failed;
    }


    private function validateForm(){

        if($this->validate()) {
            $user = $this->getUser();
            if ($user->validatePassword($this->current_password)) {

                if($this->status_update_pass){
                    if(!$this->password){
                        return 'Необходимо заполнить «Пароль».';
                    }elseif (!$this->password_confirm){
                        return 'Необходимо заполнить «Подтвердите пароль».';
                    }
                }

                $profile = UserProfile::findByUser();
                if (UserProfile::findOne(['telephone' => $this->telephone]) && ($profile && $profile->telephone != $this->telephone)) {
                    return 'Данный телефон уже зарегистрирован в программе лояльности.';
                } elseif (User::findOne(['username' => $this->email]) && \Yii::$app->user->identity->username != $this->email) {
                    return 'Учётная запись с таким адресом электронной почты уже существует.';
                }

            }else{
                return 'Неправильный текущий пароль';
            }

        }else{
            $error = "";
            $errors = $this->getErrors();
            foreach ($errors as $v) {
                $error .= $v[0];break;
            }

            return $error;
        }

        return false;
    }


    private function updateUser(){

        $this->_user->username = $this->email;
        $this->_user->email = $this->email;

        if($this->status_update_pass) {
            $this->_user->setPassword($this->password);
        }

        $transaction = $this->_user->getDb()->beginTransaction();
        if($this->_user->save(false)){
            return $this->updateProfile($transaction);
        }
    }


    private function updateProfile($transaction){

        if($profile = UserProfile::findOne(['user_id' => \Yii::$app->user->id])){

            $profile->fio = $this->fio;
            $profile->telephone = $this->telephone;
            $profile->date_of_birth = $this->birth_year.'-'.$this->birth_month.'-'.$this->birth_day;

            if($profile->save()){
                $transaction->commit();
                return true;
            }
        }

        return false;
    }

    private function addUserSubscribeList(){

        if($this->subscription){
            $subscribe = new Subscription();
            if(!Subscription::emailExists($this->email)){
                $subscribe->addEmail($this->email);
            }
        }
    }


    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(\Yii::$app->user->id);
        }

        return $this->_user;
    }


}