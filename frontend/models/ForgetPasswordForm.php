<?php
namespace frontend\models;

use common\models\Message;
use common\models\UserProfile;
use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use common\models\User;

/**
 * Password reset form
 */
class ForgetPasswordForm extends Model
{
    public $email;

    const success = 1;
    const failed = 0;

    /**
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email','email'],
        ];
    }

    public function forgetPassword(){

        if(! ($error = $this->validateModel())){

            $user = $this->getUser();
            $user->generatePasswordResetToken();
            if ($user->save(false)) {
                $profile = UserProfile::findOne(['user_id' => $user->id]);
                $link = Yii::$app->request->hostInfo . '/site/confirm?token=' . $user->password_reset_token;
                $this->sendUpdatePasswordInstruction($user->email, $link, $profile->fio);
                return self::success;
            }
        }

        return $error;
    }



    public function validateModel(){

        if(!$this->validateModel()){
            $error = "";
            foreach ($this->getErrors() as $v) {
                $error .= $v[0];
                break;
            }

            return $error;
        }else{

            if ($check = $this->getUser()) {
                return false;
            }else {
                return 'E-mail не зарегистрирован.';
            }
        }
    }



    private function sendUpdatePasswordInstruction($email, $link, $fio)
    {
        $data = Message::getForgetPasswordMessage();
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($email)
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['link' => $link, 'fio' => $fio]));
        return $emailSend->send();
    }



    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        return User::findOne(['email' => $this->email]);
    }


}
