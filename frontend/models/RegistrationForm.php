<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.02.2020
 * Time: 12:21
 */

namespace frontend\models;

use common\models\Message;
use common\models\User;
use common\models\UserInviteBalance;
use common\models\UserProfile;
use yii\httpclient\Client;
use Yii;
use yii\base\Model;

/**
 * This is the model class for tables "User, UserProfile".
 *
 * @property int $id
 * @property string $fio
 * @property string $telephone
 * @property string $email
 * @property string $password
 * @property string $password_confirm
 * @property int $birth_day
 * @property int $birth_month
 * @property int $birth_year
 * @property int $f_agreement
 * @property int $s_agreement
 */

class RegistrationForm extends Model
{
    public $fio;
    public $telephone;
    public $email;

    public $password;
    public $password_confirm;

    public $birth_day;
    public $birth_month;
    public $birth_year;

    public $f_agreement;
    public $s_agreement;

    const success = 1;
    const failed = 0;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'telephone', 'email', 'password', 'password_confirm'], 'required'],
            [['email'], 'email'],
            [['birth_day', 'birth_month', 'birth_year'], 'number'],

            [['password','password_confirm'], 'string', 'min' => 6, 'max' => 255],
            [['password_confirm'], 'compare', 'compareAttribute' => 'password'],

            [['telephone'], 'match', 'pattern' => '/^\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/', 'message' => 'Обязательное поле'],
            [['fio'], 'match', 'pattern' => '/[А-Яа-яЁё]/u', 'message' => 'Смените раскладку'],
            [['f_agreement', 's_agreement'], 'required', 'message' => 'Обязательное поле'],
            [['f_agreement', 's_agreement',], 'string'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Имя Фамилия',
            'telephone' => 'Номер телефона',
            'email' => 'E-mail',
            'password' => 'Пароль',
            'password_confirm' => 'Повторите пароль',
        ];
    }




    public function signUp(){

        if($this->validate()){
            if(UserProfile::findOne(['telephone' => $this->telephone])){
                return 'Данный телефон уже зарегистрирован в программе лояльности.';
            }elseif($this->getUser()){
                return 'Учётная запись с таким адресом электронной почты уже существует.';
            }else{

                if($this->saveUser()){
                    $this->sendMessageToEmail();
                    $this->sendMessageToPhoneNumber();
                    $this->login();
                    return self::success;
                }
            }
        }

        return self::failed;
    }


    private function saveUser(){

        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->generateAuthKey();
        $user->setPassword($this->password);

        $transaction = $user->getDb()->beginTransaction();
        if($user->save(false)){
           return $this->saveProfile($user->id, $transaction);
        }
    }


    private function saveProfile($user_id, $transaction){

        $profile = new UserProfile();
        $profile->user_id = $user_id;
        $profile->fio = $this->fio;
        $profile->telephone = $this->telephone;
        $profile->date_of_birth = $this->birth_year.'-'.$this->birth_month.'-'.$this->birth_day;

        if($profile->save()){

            $user_invite_balance = new UserInviteBalance();
            if($user_invite_balance->createNew($user_id)){
                $transaction->commit();
                return true;
            }
        }

        return false;
    }


    private function sendMessageToEmail(){

        $data = Message::getRegisterMessage();
        $message =  Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($this->email)
            ->setSubject($data->subject)
            ->setHtmlBody(Yii::t('app', $data->message,
                ['email' => $this->email, 'password' => $this->password]));

        return $message->send();
    }


    protected function sendMessageToPhoneNumber(){

        $code = $this->getRandomNumber();
        $message = "Ваш код:".$code."\nKiehl's";
        Yii::$app->session['phone_confirm_code'] = $code;
        $client = new Client();

        return $client->createRequest()
            ->setMethod('GET')
            ->setUrl('http://kazinfoteh.org:9501/api')
            ->setData([
                'action' => 'sendmessage',
                'username' => Yii::$app->params["PhoneMessageUsername"],
                "password" => Yii::$app->params["PhoneMessagePassword"],
                "recipient" => $this->getTelephone(),
                "messagetype" => "SMS:TEXT",
                "originator" => "INFO_KAZ",
                "messagedata" => $message,])
            ->send();
    }

    protected function getRandomNumber()
    {
       return rand(1000,9999);
    }


    protected function getTelephone(){
        $telephone = preg_replace('/[^a-zA-Z0-9]/','', $this->telephone);
        return '7'.substr($telephone, 1);
    }


    public function login()
    {
        if($user = $this->getUser()){
            return Yii::$app->user->login($user);
        }

    }


    protected function getUser()
    {
        return User::findByUsername($this->email);
    }

}

