<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.02.2020
 * Time: 12:21
 */

namespace frontend\models;

use common\models\Message;
use common\models\User;
use common\models\UserProfile;
use yii\httpclient\Client;
use Yii;
use yii\base\Model;

/**
 * This is the model class for tables "User, UserProfile".
 *
 * @property int $code
 */

class TelephoneConfirmForm extends Model
{
    public $code;

    const success = 1;
    const failed = 0;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['code', 'required','message' => 'Обязательное поле'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Код подтверждения',
        ];
    }


    public function confirm(){

        if($this->validate()){

            $code = Yii::$app->session['phone_confirm_code'];
            Yii::$app->session->remove('phone_confirm_code');

            if($code == $this->code){
                if($profile = UserProfile::findOne(['user_id' => Yii::$app->user->id])){
                    $profile->confirmPhone();
                    return self::success;
                }
            }

            return self::failed;

        }else{

            $error = "";
            $errors = $this->getErrors();
            foreach ($errors as $v) {
                $error .= $v[0];break;
            }

            return $error;
        }

    }


}

