<?php

use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="main-container col1-layout" id="main-container">
    <div class="main">

        <?=$this->render('/partials/breadcrumbs', ['page' => 'Account']);?>

        <div class="col-main">
            <div id="myAccount" class="my-account">

                <?=$this->render('/partials/account_menu');?>

                <div class="my-account-content">
                    <div class="my-account-dashboard">

                        <ul class="messages" id="account_error" style="display: none;">
                            <li class="error-msg">
                                <ul>
                                    <li>
                                        <span id="account_error_text">

                                        </span>
                                    </li>
                                </ul>
                            </li>
                        </ul>


                        <? if(Yii::$app->session->hasFlash('questionnaire_success_msg')):?>
                            <ul class="messages" style="border: 1px solid #21c400;">
                                <li class="success-msg" style="color: #21c400;">
                                    <ul>
                                        <li>
                                            <span>
                                              <?=Yii::$app->session->getFlash('questionnaire_success_msg');?>
                                            </span>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        <? endif;?>

                        <?= Yii::$app->session->getFlash('social_network_error');?>

                        <div class="my-account-dashboard-block">
                            <header class="my-account-dashboard-block__header my-account-dashboard-block__header_no-border">
                                <div class="my-account-dashboard-block__title">
                                    Личная информация
                                </div>

                                <a title="Редактировать"
                                   class="my-account-dashboard-block__link my-account-dashboard-block__link_right hide-on-mobile js-account-fancybox-link"
                                   data-popup="#accountEditInfo" href="#">
                                    Редактировать учётную запись
                                </a>
                                <a title="Редактировать"
                                   class="my-account-dashboard-block__link my-account-dashboard-block__link_right only-mobile js-account-fancybox-link"
                                   data-popup="#accountEditInfo" href="#">
                                    Редактировать
                                </a>
                            </header>
                            <div class="my-account-dashboard-block__body">
                                <ul class="my-account-dashboard-block-list">
                                    <li class="my-account-dashboard-block-list__item">
                                        <b class="my-account-dashboard-block-list__label">Имя Фамилия:</b>
                                        <?=$profile->fio;?>
                                    </li>
                                    <li class="my-account-dashboard-block-list__item">
                                        <b class="my-account-dashboard-block-list__label">E-mail:</b>
                                        <?=Yii::$app->user->identity->email;?>
                                    </li>
                                    <li class="my-account-dashboard-block-list__item">
                                        <b class="my-account-dashboard-block-list__label">Дата рождения:</b>
                                        <?=$profile->getBirthdayDate();?>
                                    </li>
                                    <li class="my-account-dashboard-block-list__item">
                                        <b class="my-account-dashboard-block-list__label">Телефон:</b>
                                        <?=$profile->telephone;?>
                                    </li>
                                    <li class="my-account-dashboard-block-list__item">
                                        <div class="social-for-register-wr hide-on-mobile">
                                            <div class="social-for-register__text">
                                                Нажмите на иконку, чтобы привязать соцсеть
                                            </div>
                                            <div class="social-for-register__item <?=isset($auth['facebook']) ? 'social-for-register__item_active' : '';?>">
                                                <a class="social-for-register__link social-for-register__link_fb"
                                                   href="<?=isset($auth['facebook']) ? \yii\helpers\Url::toRoute(['/account/delete-social-authentication', 'source' => 'facebook']) :
                                                       \yii\helpers\Url::toRoute(['/account/add-social-authentication', 'source' => 'facebook']);?>">
                                                    <span class="social-for-register__triangle">
                                                        <span class="social-for-register__triangle-img"></span>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="social-for-register__item <?=isset($auth['vkontakte']) ? 'social-for-register__item_active' : '';?>">
                                                <a class="social-for-register__link social-for-register__link_vk"
                                                   href="<?=isset($auth['vkontakte']) ? \yii\helpers\Url::toRoute(['/account/delete-social-authentication', 'source' => 'vkontakte']) :
                                                       \yii\helpers\Url::toRoute(['/account/add-social-authentication', 'source' => 'vkontakte']);?>">
                                                    <span class="social-for-register__triangle">
                                                        <span class="social-for-register__triangle-img"></span>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="social-for-register__item">
                                                <a class="social-for-register__link js-social-for-register social-for-register__link_instagram"
                                                   href="https://api.instagram.com/oauth/authorize?client_id=35f968fe2a6149e99a89e98dfb3214f5&amp;scope=basic&amp;redirect_uri=https%3A%2F%2Fkiehls.ru%2Fsocial%2Finstagram%2Flogin%3Ftype%3Dlink&amp;response_type=code">
														<span class="social-for-register__triangle">
															<span class="social-for-register__triangle-img"></span>
														</span>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>


                                <?php $form = ActiveForm::begin(['method' => 'post', 'id' => 'uploadAvatarForm', 'class' => 'my-account-dashboard-avatar']); ?>



                                <?= $form->field($avatarUpload, 'avatar')->fileInput(
                                    [ 'id' => 'uploadMyAvatarInput', 'class' => 'hidden'])->label(false);?>


                                    <label for="uploadAvatarInput" class="my-account-dashboard-avatar__img-wr">
                                        <img id="accountAvatar" class="my-account-dashboard-avatar__img" src="<?=$profile->getAvatar();?>"/>
                                    </label>
                                    <div class="my-account-dashboard-avatar__body">
                                        <? if($profile->avatar != null && file_exists($profile->folder.$profile->avatar)): ?>
                                        <div class="my-account-dashboard-avatar__link-w">
                                            <a id="deleteAvatarLink" class="my-account-dashboard-avatar__link" href="/account/delete-avatar">
                                                Удалить аватарку
                                            </a>
                                        </div>
                                        <? endif;?>
                                        <div class="my-account-dashboard-avatar__link-w">
                                            <a id="uploadMyAvatarLink" class="my-account-dashboard-avatar__link" href="#">
                                                Загрузить аватарку
                                            </a>
                                        </div>
                                        <div class="my-account-dashboard-avatar__info">
                                            <?= Yii::t('account', 'Файл должен быть в форматах .png, .jpg или .jpeg и размером менее 4 mb.'); ?>
                                        </div>
                                    </div>
                                <?php ActiveForm::end(); ?>

                                <div class="social-for-register-wr only-mobile">
                                    <div class="social-for-register__text">
                                        <?= Yii::t('account', ' Нажмите на иконку, чтобы привязать соцсеть'); ?>
                                    </div>
                                    <div class="social-for-register__item <?=isset($auth['facebook']) ? 'social-for-register__item_active' : '';?>">
                                        <a class="social-for-register__link social-for-register__link_fb"
                                           href="<?=isset($auth['facebook']) ? \yii\helpers\Url::toRoute(['/account/delete-social-authentication', 'source' => 'facebook']) :
                                               \yii\helpers\Url::toRoute(['/account/add-social-authentication', 'source' => 'facebook']);?>">
                                                    <span class="social-for-register__triangle">
                                                        <span class="social-for-register__triangle-img"></span>
                                                    </span>
                                        </a>
                                    </div>

                                    <div class="social-for-register__item <?=isset($auth['vkontakte']) ? 'social-for-register__item_active' : '';?>">
                                        <a class="social-for-register__link social-for-register__link_vk"
                                           href="<?=isset($auth['vkontakte']) ? \yii\helpers\Url::toRoute(['/account/delete-social-authentication', 'source' => 'vkontakte']) :
                                               \yii\helpers\Url::toRoute(['/account/add-social-authentication', 'source' => 'vkontakte']);?>">
                                                    <span class="social-for-register__triangle">
                                                        <span class="social-for-register__triangle-img"></span>
                                                    </span>
                                        </a>
                                    </div>
                                    <div class="social-for-register__item">
                                        <a class="social-for-register__link js-social-for-register social-for-register__link_instagram"
                                           href="https://api.instagram.com/oauth/authorize?client_id=35f968fe2a6149e99a89e98dfb3214f5&amp;scope=basic&amp;redirect_uri=https%3A%2F%2Fkiehls.ru%2Fsocial%2Finstagram%2Flogin%3Ftype%3Dlink&amp;response_type=code">
												<span class="social-for-register__triangle">
													<span class="social-for-register__triangle-img"></span>
												</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>




<div id="accountEditInfo" class="my-account-popup my-account-popup_border">
    <header class="my-account-popup__header">
        Персональная информация
    </header>
    <div class="my-account-popup__content">

        <?php $form = ActiveForm::begin(['method' => 'post', 'action' => '/account/index',
            'class' => 'form form_validation-process', 'id' => 'updateProfileForm']); ?>
        <div class="form__required-msg">Обязательные поля</div>
        <div class="form__row form__row_account-popup">

            <?= $form->field($model, 'fio', ['labelOptions' => [ 'class' => 'form__label form__label_bold form__label_required' ]])
                ->textInput(['id' => 'firstname', 'name' => 'UpdateProfileForm[fio]', 'class' => 'form__input', 'value' => $profile->fio,
                    'type' => 'text', 'maxlength' => 35, 'placeholder' => 'Имя Фамилия', 'title' => 'Имя Фамилия'])
                ->label('Имя Фамилия') ?>
        </div>
        <div class="form__row form__row_account-popup">
            <div class="form__cell">

                <?= $form->field($model, 'email', ['labelOptions' => [ 'class' => 'form__label form__label_bold form__label_required' ]])
                    ->textInput(['name' => 'UpdateProfileForm[email]', 'id' => 'email', 'value' => Yii::$app->user->identity->email,
                        'class' => 'form__input', 'type' => 'email',  'placeholder' => 'Адрес электронной почты (email)', 'title' => 'E-mail'])
                    ->label('E-mail') ?>
            </div>
            <div class="form__cell">
                <?= $form->field($model, 'telephone', ['labelOptions' => [ 'class' => 'form__label form__label_bold' ]])
                    ->textInput(['id' => 'registration_phone', 'name' => 'UpdateProfileForm[telephone]', 'class' => 'form__input js-phone-number',
                        'type' => 'text',  'autocomplete' => 'tel', 'placeholder' => '+7 (___) ___-__-__', 'title' => 'Телефон', 'value' => $profile->telephone])
                    ->label('Номер телефона') ?>
            </div>
        </div>
        <div class="form__row form__row_dob">
            <div class="form__dob-title">
                <span class="form__label">Дата рождения</span>
            </div>
            <select id="dob_day" class="select select_day" name="UpdateProfileForm[birth_day]" >
                <option value="0">День</option>
                <? for ($i = 1;$i <= 31;$i++):?>
                    <option value="<?=$i;?>" <?=date('d', strtotime($profile->date_of_birth)) == $i ? "selected" : "";?>>
                        <?=$i;?>
                    </option>
                <? endfor;?>
            </select>

            <select id="dob_month" class="select select_month" name="UpdateProfileForm[birth_month]" >
                <option value="0">Месяц</option>
                <? for ($i = 1;$i <= 12;$i++):?>
                    <option value="<?=$i;?>" <?=date('m', strtotime($profile->date_of_birth)) == $i ? "selected" : "";?>>
                        <?=$i;?>
                    </option>
                <? endfor;?>
            </select>

            <select id="dob_year" class="select select_year" name="UpdateProfileForm[birth_year]" >
                <option value="0">Год</option>
                <? for ($i=date('Y');$i>=date('Y')-110;$i--):?>
                    <option value="<?=$i;?>" <?=date('Y', strtotime($profile->date_of_birth)) == $i ? "selected" : "";?>>
                        <?=$i;?>
                    </option>
                <? endfor;?>
            </select>
        </div>

        <div class="form__row form__row_account-popup">
            <?= $form->field($model, 'subscription', [
                'template' => '{input}{label}{error}',
                'labelOptions' => [ 'class' => 'checkbox-label' ]
            ])->textInput(['id' => 'subscription', 'name' => 'UpdateProfileForm[subscription]',
                'class' => 'checkbox input_checkbox', 'type' => 'checkbox'])
                ->label( 'Подписаться на рассылку Kiehl\'s (Email/SMS)') ?>

            <div class="my-account-popup__aside-text">
                <?= Yii::t('account', 'Новости, акции, смена статуса заказа и многое другое.'); ?>
                <br />
            </div>
        </div>
        <div class="form__row form__row_account-popup form__row_no-margin">
            <div class="form__cell">
                <div class="form__row">
                    <?= $form->field($model, 'current_password', ['labelOptions' => [ 'class' => 'form__label form__label_bold form__label_required' ]])
                        ->textInput(['id' => 'current_password', 'name' => 'UpdateProfileForm[current_password]',
                            'class' => 'form__input', 'type' => 'password', 'placeholder' => 'Текущий пароль', 'title' => 'Текущий пароль'])
                        ->label('Введите текущий пароль') ?>
                </div>
            </div>
        </div>
        <div class="form__row form__row_account-popup">
            <div class="form__cell">
                <?= $form->field($model, 'status_update_pass', [
                    'template' => '{input}{label}{error}',
                    'labelOptions' => [ 'class' => 'checkbox-label' ]
                ])->textInput(['id' => 'change_password', 'name' => 'UpdateProfileForm[status_update_pass]',
                    'class' => 'checkbox', 'type' => 'checkbox'])
                    ->label( 'Сменить пароль') ?>
            </div>
        </div>
        <div id="accountEditInfoFormExtra" class="_hidden">
            <div class="form__row form__row_account-popup">
                <div class="form__cell">
                    <?= $form->field($model, 'password', ['labelOptions' => [ 'class' => 'form__label form__label_bold form__label_required' ]])
                        ->textInput(['id' => 'password', 'name' => 'UpdateProfileForm[password]',
                            'class' => 'form__input', 'type' => 'password', 'placeholder' => 'Новый пароль', 'title' => 'Новый пароль'])
                        ->label('Новый пароль') ?>
                </div>
            </div>
            <div class="form__row form__row_account-popup">
                <div class="form__cell">

                    <?= $form->field($model, 'password_confirm', ['labelOptions' => [ 'class' => 'form__label form__label_bold form__label_required' ]])
                        ->textInput(['id' => 'password', 'name' => 'UpdateProfileForm[password_confirm]',
                            'class' => 'form__input', 'type' => 'password', 'placeholder' => 'Подтвердите пароль', 'title' => 'Подтвердите новый пароль'])
                        ->label('Подтвердите пароль') ?>
                </div>
            </div>
        </div>

        <?= Html::submitButton('Сохранить', ['class' => 'btn btn_full', 'title' => 'Сохранить']) ?>
        <?php ActiveForm::end(); ?>

    </div>
</div>



