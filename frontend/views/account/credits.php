<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="main-container col1-layout" id="main-container">
    <div class="main">

        <?=$this->render('/partials/breadcrumbs', ['page' => 'КЛУБ ДРУЗЕЙ KIEHL\'S']);?>

        <div class="col-main">
            <div id="myAccount" class="my-account">

                <?=$this->render('/partials/account_menu');?>

                <div class="loyalty js-loyalty my-account-content">
                    <div class="loyalty-account-header">
                        <img src="https://kiehls.ru/skin/frontend/kiehls/default/img/loyalty/loyalty-account.jpg" />
                        <div class="loyalty-account-header__text">
                            <?=Yii::t('account', 'Привет, {fio} !', ['fio' => $profile->fio]);?>
                        </div>
                    </div>

                <? if($profile->telephone != null && $profile->phone_confirm != 0):?>

                        <div class="loyalty-account-current">
                            <div class="loyalty-account-current__value">
                                <?=Yii::t('account', 'Текущий баланс {count}', ['count' => '<span>0 баллов</span>']);?>
                            </div>
                        </div>
                        <div class="loyalty-account-convert">
                            <?= Yii::t('account', 'Накопите 1000 баллов, чтобы начать конвертировать их в купоны'); ?>
                        </div>
                        <div class="loyalty-account-progress">
                                <span class="loyalty-account-progress__txt">
                                     <?= Yii::t('account', 'Вам осталось еще {count} баллов', ['count' => '1000']); ?>
                                </span>
                            <div data-current-value="" class="loyalty-account-progress__bar js-loyalty-account-progress"
                                 data-max-value="1000" style="width: 0%;"></div>
                        </div>
                        <ul id="loyaltyAccordion" class="loyalty-accordion">
                            <? foreach ($faq as $v):?>
                                <li class="js-cms-accordion-container loyalty-accordion__item">
                                    <header class="js-cms-accordion-trigger loyalty-accordion__header">
                                        <?=$v->title;?>
                                    </header>
                                    <div class="js-cms-accordion-content loyalty-accordion__body">
                                        <? if($v->have_some_content):?>
                                            <div class="loyalty-accordion__text"> <?=$v->content;?></div>
                                            <div class="loyalty-history js-loyalty-history">
                                                <div class="loyalty-history-filter">
                                                    <form class="js-loyalty-history-form" method="get"
                                                          action="/loyaltyprogram/account">
                                                        <div class="loyalty-history-filter__date">
                                                            <span class="loyalty-history-filter__preposition">c</span>
                                                            <input type="date"
                                                                   class="loyalty-history-filter__input js-loyalty-date hasDatepicker"
                                                                   name="from" id="loyaltyFrom" value="" />
                                                        </div>
                                                        <div class="loyalty-history-filter__date">
                                                            <span class="loyalty-history-filter__preposition">по</span>
                                                            <input type="date"
                                                                   class="loyalty-history-filter__input js-loyalty-date hasDatepicker" name="to"
                                                                   id="loyaltyTo" value="" />

                                                        </div>
                                                    </form>
                                                </div>
                                                <p class="loyalty__help">
                                                    Баллы накапливаются за счет совершаемых Вами покупок. С
                                                    каждых
                                                    <b>200 <span class="rouble">a</span></b>
                                                    Вы получаете
                                                    <b>10 баллов</b> на ваш баланс.
                                                </p>
                                            </div>
                                        <? else:?>
                                            <?=$v->content;?>
                                        <? endif;?>
                                    </div>
                                </li>
                            <? endforeach;?>
                        </ul>

                <? else:?>


                        <div class="loyalty__header">
                            <?= Yii::t('account', 'присоединяйтесь к клубу друзей KIEHL’S'); ?></div>
                        <div class="loyalty__content form">
                            <?php $form = ActiveForm::begin(['method' => 'post', 'class' => 'loyalty__hidden js-loyalty-phone', 'action' => '/account/credit']); ?>

                                <div class="form__row">
                                    <div class="form__row">
                                        <?= $form->field($model, 'telephone', ['labelOptions' => [ 'class' => 'loyalty__label loyalty__label_phone' ]])
                                            ->textInput(['id' => 'loyalty_phone', 'name' => 'TelephoneForm[telephone]', 'class' => 'form__input',
                                                'type' => 'text', 'placeholder' => '+7 (___) ___-__-__', 'title' => 'Телефон'])
                                            ->label('Номер телефона') ?>
                                    </div>
                                </div>
                                <div class="form__row _account-loyalty-agreement" >
                                        <?= $form->field($model, 'agreement', [
                                            'template' => '{input}{label}{error}',
                                            'labelOptions' => [ 'class' => 'loyalty__label' ]
                                        ])->textInput(['id' => 'is_subscribed_page', 'name' => 'TelephoneForm[agreement]', 'class' => 'checkbox input_checkbox',
                                            'type' => 'checkbox', 'title' => $content->f_agreement])
                                            ->label($content->f_agreement) ?>
                                </div>
                                <div class="form__row">
                                    <?= Html::submitButton('подтвердить номер', ['class' => 'btn btn_full', 'id' => 'btnSignUp', 'title' => 'Создать аккаунт']) ?>
                                </div>
                            <?php ActiveForm::end(); ?>

                        </div>
                        <div class="loyalty__footer">
                            <?= Yii::t('account', 'Получайте баллы с каждой покупкой и оплачивайте ими<br> до 100% стоимости вашей покупки'); ?>
                        </div>

                <? endif;?>

                </div>
            </div>
        </div>
    </div>
</div>

