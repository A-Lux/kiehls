<?php

use yii\helpers\Url;

?>
<div class="main-container col1-layout" id="main-container">
    <div class="main">

        <?=$this->render('/partials/breadcrumbs', ['page' => 'Пригласить друзей']);?>

        <div class="col-main">
            <div id="myAccount" class="my-account">

                <?=$this->render('/partials/account_menu');?>

                <section class="my-account-invitation my-account-content">
                    <div class="my-account-invitation__points-panel">
                        <?=Yii::t('account', 'Количество накопленных баллов за приглашение друга {count}', ['count' => '<span>'.$balance.'</span>']);?>

                    </div>
                    <h1 class="my-account-invitation__title">

                        <?= Yii::t('account', "Друзей Kiehl's должно&nbsp;быть&nbsp;много!"); ?>

                    </h1>
                    <div class="my-account-invitation__text">
                        <?= Yii::t('account', "Вы получите <b>100 баллов</b> за каждого приведенного друга
                        в программу лояльности
                        \"Клуб Друзей Kiehl's\", после того, как он
                        зарегистрируется<br />
                        в интернет-магазине kiehls.gip-trans-group.kz и совершит свою
                        первую покупку*"); ?>
                    </div>
                    <div class="my-account-invitation-types">
                        <div class="my-account-invitation-type">
                            <div class="my-account-invitation-type__title">
                                <?= Yii::t('account', 'Пригласить друга через соцсети'); ?>
                            </div>
                            <div class="my-account-invitation-social">
                                <a class="my-account-invitation-social__btn js-account-invite-social-link" target="_blank" rel="nofollow"
                                   href="https://vk.com/share.php?url=<?= $invite_url;?>&title=<?=$title;?>&image=<?=$img;?>" title="VK.COM">
                                    <span class="footer-social__img footer-social__img_vk"></span>
                                </a>
                                <a class="my-account-invitation-social__btn js-account-invite-social-link" target="_blank" rel="nofollow"
                                   href="https://www.facebook.com/sharer/sharer.php?u=<?= $invite_url;?>" title="Facebook">
                                    <span class="footer-social__img footer-social__img_fb"></span>
                                </a>
                            </div>
                        </div>
                        <div class="my-account-invitation-type">
                            <div class="my-account-invitation-type__title">
                                <?= Yii::t('account', 'Отправить приглашение любым другим способом, скопировав ссылку'); ?>
                            </div>
                            <div class="my-account-invitation-personal">
                                <input type="text" class="my-account-invitation-personal__input js-account-invite-input form__input"
                                       value="<?=$invite_url;?>" />
                                <button class="btn js-account-invite-copy" type="button">
                                    Скопировать ссылку
                                </button>
                            </div>
                            <p class="my-account-invitation__copy-link js-show-text">
                                ссылка скопирована
                            </p>
                        </div>
                    </div>
                    <div class="my-account-invitation__note">
                        <?= Yii::t('account', '*Акция "Пригласи друга" действует только для&nbsp;участников программы лояльности "Клуб&nbsp;Друзей".'); ?>

                    </div>
                    <div class="my-account-invitation__note">
                        <?= Yii::t('account', 'В случае совершения покупки в бутике баллы будут начислены на следующий день.'); ?>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>