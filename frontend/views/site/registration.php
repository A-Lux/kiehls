<?php


?>
<div class="main-container col1-layout" id="main-container">
    <div class="main">
        <div class="col-main">
            <div class="wrapper">
                <div class="account-create">
                    <div class="account-create__i">
                        <div class="account-create__title"><?=$content->title;?></div>
                        <div class="account-create__subtitle">
                            <span class="have-account">А если аккаунт уже есть -
                                <a href="<?=yii\helpers\Url::toRoute(['/sign-in'])?>" class="link js-fancybox-popup">жмите&nbsp;сюда</a></span></div>
                        <div class="account-create__content">
                            <div class="account-create__loyalty">
                                <div class="account-create__loyalty-title"> <?=$content->rectangle_title;?></div>
                                <div class="account-create__loyalty-logo"> <img
                                        src="<?=$content->getImage();?>" alt="">
                                </div>
                                <div class="account-create__loyalty-description"> <?=$content->rectangle_desc;?></div>
                            </div>
                            <div class="account-create__form">
                                <?php $form = yii\widgets\ActiveForm::begin(['method' => 'post', 'id' => 'signUpForm', 'action' => '/sign-up']); ?>

                                    <div class="form__row">
                                        <?= $form->field($model, 'fio', ['labelOptions' => [ 'class' => 'form__label' ]])
                                            ->textInput(['id' => 'firstnamePage', 'name' => 'RegistrationForm[fio]', 'class' => 'form__input',
                                            'type' => 'text', 'maxlength' => 35, 'autocomplete' => 'name', 'placeholder' => 'Имя Фамилия', 'title' => 'Имя Фамилия'])
                                            ->label('Имя Фамилия') ?>
                                    </div>

                                    <div class="form__row">
                                        <?= $form->field($model, 'telephone', ['labelOptions' => [ 'class' => 'form__label' ]])
                                            ->textInput(['id' => 'registration_phone', 'name' => 'RegistrationForm[telephone]', 'class' => 'form__input js-phone-number valid',
                                            'type' => 'text', 'placeholder' => '+7 (___) ___-__-__', 'title' => 'Телефон'])
                                            ->label('Номер телефона') ?>
                                    </div>

                                    <div class="form__row">
                                        <?= $form->field($model, 'email', ['labelOptions' => [ 'class' => 'form__label' ]])
                                            ->textInput(['name' => 'RegistrationForm[email]',
                                            'class' => 'form__input', 'type' => 'email',  'placeholder' => 'E-mail', 'title' => 'E-mail'])
                                            ->label('E-mail') ?>
                                    </div>

                                    <br><br>

                                    <div class="form__row">
                                        <?= $form->field($model, 'password', ['labelOptions' => [ 'class' => 'form__label' ]])
                                            ->textInput(['id' => 'password', 'name' => 'RegistrationForm[password]',
                                                'class' => 'form__input', 'type' => 'password', 'placeholder' => 'Пароль', 'title' => 'Пароль'])
                                            ->label('Пароль') ?>
                                    </div>

                                    <div class="form__row">
                                        <?= $form->field($model, 'password_confirm', ['labelOptions' => [ 'class' => 'form__label' ]])
                                            ->textInput(['id' => 'confirmationPage', 'name' => 'RegistrationForm[password_confirm]',
                                                'class' => 'form__input', 'type' => 'password', 'placeholder' => 'Повторите пароль', 'title' => 'Повторите пароль'])
                                            ->label('Повторите пароль') ?>
                                    </div>

                                    <div class="form__row form__row_dob">
                                        <div class="form__dob-title">
                                            <span class="form__label">Дата рождения</span>
                                        </div>


                                        <select id="dob_day" class="select select_day" name="RegistrationForm[birth_day]" >
                                            <option value="0">День</option>
                                            <? for ($i = 1;$i <= 31;$i++):?>
                                                <option value="<?=$i;?>" <?=$model->birth_day == $i ? "selected" : "";?>>
                                                    <?=$i;?>
                                                </option>
                                            <? endfor;?>
                                        </select>

                                        <select id="dob_month" class="select select_month" name="RegistrationForm[birth_month]" >
                                            <option value="0">Месяц</option>
                                            <? for ($i = 1;$i <= 12;$i++):?>
                                                <option value="<?=$i;?>" <?=$model->birth_month == $i ? "selected" : "";?>>
                                                    <?=$i;?>
                                                </option>
                                            <? endfor;?>
                                        </select>

                                        <select id="dob_year" class="select select_year" name="RegistrationForm[birth_year]" >
                                            <option value="0">Год</option>
                                            <? for ($i=date('Y');$i>=date('Y')-110;$i--):?>
                                                <option value="<?=$i;?>" <?=$model->birth_month == $i ? "selected" : "";?>>
                                                    <?=$i;?>
                                                </option>
                                            <? endfor;?>
                                        </select>
                                    </div>

                                    <div class="form__row">
                                        <?= $form->field($model, 'f_agreement', [
                                            'template' => '{input}{label}{error}',
                                            'labelOptions' => [ 'class' => 'form__label form__label_agreement' ]
                                        ])->textInput(['id' => 'is_subscribed_page', 'name' => 'RegistrationForm[f_agreement]',
                                            'class' => 'checkbox input_checkbox', 'type' => 'checkbox', 'title' => 'Я согласен с условиями Клуба друзей Kiehl’s и хочу получать новости от Kiehl\'s'])
                                            ->label( $content->f_agreement) ?>
                                    </div>

                                    <div class="form__row">
                                        <?= $form->field($model, 's_agreement', [
                                            'template' => '{input}{label}{error}',
                                            'labelOptions' => [ 'class' => 'form__label form__label_agreement' ]
                                        ])->textInput(['id' => 'signupAgreementPage', 'name' => 'RegistrationForm[s_agreement]',
                                            'class' => 'checkbox input_checkbox', 'type' => 'checkbox', 'title' => 'Хочу подписаться на рассылку.'])
                                            ->label( $content->s_agreement) ?>
                                    </div>

                                    <div class="form__row">
                                        <?= yii\helpers\Html::submitButton('Создать аккаунт', ['class' => 'btn btn_full btn_submit', 'id' => 'btnSignUp', 'title' => 'Создать аккаунт']) ?>
                                    </div>

                                <?php yii\widgets\ActiveForm::end(); ?>
                            </div>
                        </div>
                        <div class="account-create__note"> <?=$content->footer_content;?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="privacy-content-popup" id="privacyContentPopup" style="display:none;">
    <h3 class="cms-page__header"><?=$agreement->title;?></h3>
    <?=$agreement->content;?>
</div>







