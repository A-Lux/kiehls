<div class="main-container col1-layout" id="main-container">
    <div class="main">
        <div class="col-main">
            <div class="core">
                <div class="wrapper">
                    <div class="std">
                        <div class="loyalty-welcome">
                            <div class="loyalty-welcome__content">
                                <h2 class="loyalty-welcome__title"><?=$content->title;?></h2>
                                <h3 class="loyalty-welcome__subtitle"><?=$content->subtitle;?></h3>
                                <p class="loyalty-welcome__text"><?=$content->description;?></p>
                                <p class="loyalty-welcome__text_red"> <?=$content->red_text;?></p> <a
                                    href="<?=\yii\helpers\Url::toRoute('/sign-up');?>" class="btn">Вступить в клуб</a>
                            </div>
                            <div class="loyalty-circle">
                                <div class="loyalty-circle__img"> <img
                                        src="https://kiehls.ru/skin/frontend/kiehls/default/img/loyalty/loyalty-circle.jpg">
                                </div>
                                <div class="loyalty-circle__content">
                                    <div class="loyalty-circle__row loyalty-circle__row_first">
                                        <div class="loyalty-circle__item">
                                            <div class="loyalty-circle__title"><?=$processes[1]->title;?></div>
                                            <div class="loyalty-circle__text"><?=$processes[1]->content;?></div>
                                        </div>
                                        <div class="loyalty-circle__item">
                                            <div class="loyalty-circle__title"><?=$processes[2]->title;?></div>
                                            <div class="loyalty-circle__text"><?=$processes[2]->content;?></div>
                                        </div>
                                    </div>
                                    <div class="loyalty-circle__row loyalty-circle__row_second">
                                        <div class="loyalty-circle__item">
                                            <div class="loyalty-circle__title loyalty-circle__title_gold"><?=$processes[0]->title;?></div>
                                            <div class="loyalty-circle__text"><?=$processes[0]->content;?></strong></div>
                                        </div>
                                        <div class="loyalty-circle__item">
                                            <div class="loyalty-circle__title"><?=$processes[3]->title;?></div>
                                            <div class="loyalty-circle__text"><?=$processes[3]->content;?></div>
                                        </div>
                                    </div>
                                    <div class="loyalty-circle__row loyalty-circle__row_third">
                                        <div class="loyalty-circle__item">
                                            <div class="loyalty-circle__title"><?=$processes[4]->title;?></div>
                                            <div class="loyalty-circle__text"><?=$processes[4]->content;?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>