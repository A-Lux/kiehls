

<div class="main-container col1-layout" id="main-container">
    <div class="main">
        <div class="col-main">
            <div class="wrapper">
                <div class="account-create">
                    <div class="account-create__i">

                        <?php if (Yii::$app->session->getFlash('social_network_error')) : ?>
                            <ul class="messages" id="error">
                                <li class="error-msg">
                                    <ul>
                                        <li>
                                            <span><?= Yii::$app->session->getFlash('social_network_error'); ?></span>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        <?php endif; ?>

                        <div class="account-login__title"><span>Вход</span></div>
                        <div class="account-create__subtitle">
                            <span class="have-account">Нет аккаунта? -
                                <a href="<?=yii\helpers\Url::toRoute(['/sign-up'])?>" class="link js-fancybox-popup">жмите&nbsp;сюда</a>
                            </span>
                        </div>

                        <div class="account-create__content" >

                            <div class="account-create__form" style="margin: 0 auto;">

                                <div class="account-login-popup__social">
                                    <a class="social-for-register fb"
                                       href="<?=\yii\helpers\Url::toRoute(['/site/auth', 'authclient' => 'facebook'])?>" title="Facebook"></a>
                                    <a class="social-for-register vk"
                                       href="<?=\yii\helpers\Url::toRoute(['/site/auth', 'authclient' => 'vkontakte'])?>" title="VKontakte">
                                    </a>
                                    <a class="social-for-register instagram"
                                       href="https://api.instagram.com/oauth/authorize?client_id=35f968fe2a6149e99a89e98dfb3214f5&amp;scope=basic&amp;redirect_uri=https%3A%2F%2Fkiehls.ru%2Fsocial%2Finstagram%2Flogin&amp;response_type=code">
                                    </a>
                                </div>

                                <?php $form = yii\widgets\ActiveForm::begin(['class' => 'form account-login form_validation-process', 'method' => 'post', 'id' => 'login-form', 'action' => '/sign-in']); ?>
                                <div class="account-login__i">
                                    <div class="form__row">
                                        <?= $form->field($model, 'username', ['labelOptions' => [ 'class' => 'form__label' ]])
                                            ->textInput(['name' => 'LoginForm[username]', 'class' => 'form__input',
                                                'type' => 'email', 'autocomplete' => 'email', 'placeholder' => 'E-mail', 'title' => 'E-mail'])
                                            ->label('E-mail:') ?>
                                    </div>
                                    <div class="form__row">
                                        <?= $form->field($model, 'password', ['labelOptions' => [ 'class' => 'form__label' ]])
                                            ->textInput([ 'name' => 'LoginForm[password]', 'class' => 'form__input',
                                                'type' => 'password', 'placeholder' => 'Пароль', 'title' => 'Пароль'])
                                            ->label('Пароль:') ?>
                                    </div>
                                    <div class="form__row">
                                        <div class="units-row end">
                                            <div class="unit-50">
                                                <?= $form->field($model, 'rememberMe', [
                                                    'template' => '{input}{label}{error}',
                                                    'labelOptions' => [ 'class' => 'checkbox-label' ]
                                                ])->textInput(['id' => 'remember_me_signin', 'name' => 'RegistrationForm[rememberMe]',
                                                    'class' => 'checkbox', 'type' => 'checkbox', 'title' => 'Запомнить меня'])
                                                    ->label( 'Запомнить меня') ?>
                                            </div>
<!--                                            <div class="unit-50 unit_right">-->
<!--                                                <a href="--><?//=yii\helpers\Url::toRoute(['/forgot-password'])?><!--"-->
<!--                                                   class="form__link">Забыли пароль?</a>-->
<!--                                            </div>-->
                                        </div>
                                    </div>
                                    <?= yii\helpers\Html::submitButton('Войти', ['class' => 'btn btn_full', 'id' => 'send2', 'type' => 'submit', 'title' => 'Войти', 'name' => 'send']) ?>
                                </div>
                                <?php yii\widgets\ActiveForm::end(); ?>
                            </div>

                        </div>
                        <div class="account-create__note"> <?=$content->footer_content;?></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



