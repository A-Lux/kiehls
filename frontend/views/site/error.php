<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $message;
?>

<div class="main-container col1-layout" id="main-container">
    <div class="main">
        <div class="col-main">
            <div class="core">
                <div class="wrapper">
                    <div class="std">
                        <div class="cms-400-error">
                            <div class="cms-400-error__header">
                                <div class="cms-400-error__text">
                                    <h3 class="cms-400-error__sub-title">
                                        <?= nl2br(Html::encode($message)) ?>
                                    </h3>
                                </div>
                                <a href="/" class="cms-400-error__button">Вернуться на главную страницу</a>
                            </div>
                            <div class="cms-400-error__content">
                                <img src="/img/img-404.webp" alt="" class="only-mobile">
                                <div class="cms-400-error__info-wrap">
                                    <? foreach (Yii::$app->view->params['404'] as $v):?>
                                        <div class="cms-400-error__info">
                                            <div class="cms-400-error__info-title"><?=$v->title;?></div>
                                            <div class="cms-400-error__info-description">
                                                <?=$v->content;?>
                                            </div>
                                        </div>
                                    <? endforeach;?>
                                </div>
                            </div>
                        </div>
                        <div class="home-product-carousel"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
