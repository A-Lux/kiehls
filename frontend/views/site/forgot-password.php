<div class="main-container col1-layout" id="main-container">
    <div class="main">
        <div class="col-main">

            <ul class="messages">
                <li class="error-msg">
                    <ul>
                        <li>
                            <span>
                              <?= Yii::$app->session->getFlash('social_network_error') ? Yii::$app->session->getFlash('social_network_error') :
                                  Yii::t('forgot-password', "Пожалуйста, введите ваш адрес электронной почты (email).");?>
                            </span>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="additional-pass-page">
                <h2 class="additional-pass-page__title">
                    <?= Yii::t('forgot-password', "Восстановление пароля"); ?>
               </h2>
                <span class="additional-pass-page__sub-title">
                     <?= Yii::t('forgot-password', "Пожалуйста, введите свой адрес электронной почты. Вы получите ссылку для восстановления пароля."); ?>
                </span>
                <form action="/site/forgot-password" method="post" class="form form_validation-process" id="forgotPasswordPageForm" novalidate="novalidate">
                    <div class="form__row">
                        <input type="email" name="email" id="email_address" class="form__input" placeholder="E-mail" value="">
                    </div>
                    <div class="form__row">
                        <button type="submit" title="Отправить" class="btn btn_full btn_sans-serif btn_bordered  disabled">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>