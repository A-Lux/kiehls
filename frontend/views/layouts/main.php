<?php
    use frontend\assets\AppAsset;
    AppAsset::register($this);
?>

<?php $this->beginPage() ?>
    <?=$this->render('_header');?>
    <?=$content?>
    <?=$this->render('_footer');?>
<?php $this->endPage() ?>