<?php

use yii\helpers\Html;

?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <?php $this->registerCsrfMetaTags() ?>
        <?php $this->head() ?>

        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="<?=Yii::$app->view->params['header']->getFavicon();?>" type="image/x-icon" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript" src="/js/all.js"></script>
    </head>
    <body>
<?php $this->beginBody() ?>

    <header class="header">
        <header class="header">
            <div class="hide-on-mobile"></div>
            <div class="top-header hide-on-mobile js-top-header" data-adwords="true" data-mindbox="1"
                 data-is-logged-in="false" >
                <div class="top-header__i">
                </div> <a class="header-logo hide-on-mobile" href="/"> <img
                            src="<?=Yii::$app->view->params['header']->getLogo();?>" alt=""> </a>
            </div>
            <div id="mobileHeader" class="only-mobile mobile-header _absolute">
                <ul class="mobile-header__list">
                    <li class="mobile-header__item"> <a class="mobile-header__link js-mobile-nav-link" href="#"> <span
                                    class="mobile-header__img mobile-header__img_menu"></span> </a></li>
                    <li class="mobile-header__item"> <a class="mobile-header__link" > <span
                                    class="mobile-header__img mobile-header__img_pin"></span> </a></li>
                    <li class="mobile-header__item mobile-header__item_big"> <a class="mobile-header__link"
                                                                                href="/"> <img class="mobile-header__logo-img"
                                                                                                                src="<?=Yii::$app->view->params['header']->getLogo();?>" width="120"
                                                                                                                height="42" alt=""> </a></li>
                    <li class="mobile-header__item"> <a class="mobile-header__link js-open-header-search" href="#"> <span
                                    class="mobile-header__img mobile-header__img_search"></span> </a></li>
                    <li class="mobile-header__item">
                        <a class="mobile-header__link mobile-header__link_cart">
                            <span class="img-wrap">
                                <span
                                        class="mobile-header__img mobile-header__img_cart">

                                </span> <span class="count" id="mobileCartIcon">0</span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>


            <?=$this->render('_modal');?>

        </header>
    </header>

