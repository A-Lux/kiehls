<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

?>



<div class="fancybox-wrap fancybox-desktop fancybox-type-html fancybox-opened" tabindex="-1" id="error"
     style="width: 350px; height: auto; position: fixed; top: 113px; left: 584px; opacity: 1;display: none; ">
    <div class="fancybox-skin" style="padding: 15px; width: auto; height: auto;">
        <div class="fancybox-outer">
            <div class="fancybox-inner" style="overflow: auto; width: 320px; height: auto;">
                <div class="success-sign-for-news-popup" id="error_text">
                    <p></p>
                </div>
            </div>
        </div>
        <a title="Закрыть" class="fancybox-item fancybox-close" onclick="$('#error').hide();"></a>
    </div>
</div>


<div class="fancybox-wrap fancybox-desktop fancybox-type-inline fancybox-opened" tabindex="-1" id="confirmTelModal"
     style="width: 370px; height: auto; position: absolute; top: 86px; left: 574px; opacity: 1;overflow: visible;
     <?=(isset(Yii::$app->session['phone_confirm_code']) && !Yii::$app->user->isGuest) ? "" : "display: none";?>">
    <div class="fancybox-skin" style="padding: 0px; width: auto; height: auto;">
        <div class="fancybox-outer">
            <div class="fancybox-inner" style="overflow: auto; width: 370px; height: auto;">
                <div class="account-create-code-popup" id="accountCreateValidationPopup" style="">
                    <div class="account-create__loyalty-logo">
                        <img src="/img/code-popup.jpg" alt="">
                    </div>
                    <div class="account-create-code-popup__title"> пожалуйста, введите код подтверждения</div>
                    <form method="post" id="accountCreateValidation" class="form_validation-process" >
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                        <div class="form__row">
                            <span class="loyalty__label loyalty__label_phone">Код подтверждения</span>
                            <input type="text" maxlength="4" name="TelephoneConfirmForm[code]" class="form__input">
                            <div class="help-block" id="confirm_code_error" style="color: #a94442;text-align: left;"></div>
                        </div>
                        <div class="form__row">
                            <button id="btnTelConfirm" class="btn btn_full" type="button">Стать участником</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <a title="Закрыть" class="fancybox-item fancybox-close" onclick="$('#confirmTelModal').hide();"></a>
    </div>
</div>


<?php if(isset(Yii::$app->params['questionnaire'])):?>
<div class="fancybox-overlay fancybox-overlay-fixed" id="questionnaireModal" style="width: auto; height: auto; display:
    <?=(isset(Yii::$app->params['questionnaire_status']) && Yii::$app->params['questionnaire_status'] && !Yii::$app->user->isGuest) ? "block" : "none";?>;" >
    <div class="fancybox-wrap fancybox-desktop fancybox-type-inline fancybox-opened fancybox-wrap_smooth" tabindex="-1"
         style="<?=isMobile() ? 'width: 335px;left: 20px;' : 'width: 700px;left: 409px;';?> height: auto; position: absolute; top: 20px;  opacity: 1; overflow: visible;">
        <div class="fancybox-skin" style="padding: 0px; width: auto; height: auto;">
            <div class="fancybox-outer">
                <div class="fancybox-inner" style="overflow: auto; <?=isMobile() ? 'width: 335px;height: 720px;' : 'width: 700px;height: 650px;';?> ">
                    <div class="privacy-content-popup" id="privacyContentPopup" style="">
                        <div class="account-create__loyalty-logo">
                            <img src="/img/code-popup.jpg" alt="">
                        </div>
                        <header class="my-account-popup__header" style="padding: 0rem 0;">
                            <?= Yii::t('questionnaire', "пожалуйста, заполните анкету!"); ?>
                        </header>
                        <form method="post" id="accountCreateValidation" class="form_validation-process"
                              action="<?=Url::toRoute(['/account/questionnaire']);?>">
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                            <? $m = 0;?>
                            <? foreach ( Yii::$app->params['questionnaire'] as $v):?>
                                <?
                                    $m++;
                                    $options = $v->answerOptions;
                                ?>
                                <div class="form__row">
                                    <span class="loyalty__label loyalty__label_phone questionnaire_title">
                                        <?=$m?>) <?=$v->content;?>
                                    </span>
                                    <? foreach ($options as $option):?>
                                        <div>
                                            <input value="<?=$option->id;?>" type="radio" class="questionnaire_option" id="a_<?=$option->id;?>" name="q_<?=$v->id;?>"
                                                   title="<?=$option->content;?>" data-id="<?=$v->id;?>" required>
                                            <label class="checkbox-label" class="questionnaire_content" for="a_<?=$option->id;?>"><?=$option->content;?></label>
                                        </div>
                                    <? endforeach;?>

                                    <? if($v->another_variant_status):?>
                                        <div>
                                            <input type="radio" class="questionnaire_another_option" id="another_<?=$v->id;?>" name="q_<?=$v->id;?>"
                                                   title="Другой вариант" data-id="<?=$v->id;?>" required>
                                            <label class="checkbox-label" for="another_<?=$v->id;?>">Другой вариант</label>
                                            <input type="text" id="another_input_<?=$v->id;?>" name="another_<?=$v->id;?>" >
                                        </div>
                                    <? endif;?>
                                </div>
                            <? endforeach;?>

                            <div class="form__row">
                                <button id="btnQuestionnaire" class="btn btn_full btn_submit" type="submit" title="Отправить">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <a title="Закрыть" class="fancybox-item fancybox-close" onclick="$('#questionnaireModal').hide();"></a>
        </div>
    </div>
</div>
<?php endif;?>




<div class="account-create-code-popup" id="accountCreateValidationPopup" style="display:none;">
    <div class="account-create__loyalty-logo"> <img
            src="https://kiehls.ru/skin/frontend/kiehls/default/img/loyalty/code-popup.jpg" alt=""></div>
    <div class="account-create-code-popup__title"> пожалуйста, введите код подтверждения</div>
    <form method="post" action="#" id="accountCreateValidation" class="form_validation-process"
          novalidate="novalidate"> <input type="hidden" name="customer_id" id="customer_id" value="">
        <div class="form__row"> <span class="loyalty__label loyalty__label_phone">Код подтверждения</span> <input
                type="text" maxlength="4" name="code" class="form__input"></div>
        <div class="form__row"> <button class="btn btn_full" type="submit">Стать участником</button></div>
    </form>
</div>