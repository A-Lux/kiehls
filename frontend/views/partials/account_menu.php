<div id="accountNav" class="my-account-nav">
    <div class="my-account__header hide-on-mobile" id="cms">
        Мой Kiehl's
    </div>
    <ul id="cmsAccordion" class="my-account-nav-list">
        <li class="js-cms-accordion-container my-account-nav-list__item">
            <header class="js-cms-accordion-trigger my-account-nav-list__header">
                <a class="" href="<?=\yii\helpers\Url::toRoute('/account/credit')?>">
                    Клуб друзей Kiehl's
                </a>
            </header>
            <div class="js-cms-accordion-content my-account-nav-list__body">
                <ul class="my-account-nav-list__text">
                    <li>

                        <a class="my-account-nav-list__link" href="<?=\yii\helpers\Url::toRoute('/account/invite')?>">
                            Пригласить друзей
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="js-cms-accordion-container my-account-nav-list__item">
            <header class="js-cms-accordion-trigger my-account-nav-list__header">
                Мой профиль
            </header>
            <div class="js-cms-accordion-content my-account-nav-list__body">
                <ul class="my-account-nav-list__text">
                    <li>
                        <a class="my-account-nav-list__link" href="/account">
                            Личная информация
                        </a>
                    </li>
<!--                    <li>-->
<!--                        <a class="my-account-nav-list__link" href="address.html">-->
<!--                            Адресная книга-->
<!--                        </a>-->
<!--                    </li>-->
                </ul>
            </div>
        </li>
        <li class="js-cms-accordion-container my-account-nav-list__item">
<!--            <header class="js-cms-accordion-trigger my-account-nav-list__header">-->
<!--                <a class="" href="list.html">-->
<!--                    Подарочные сертификаты-->
<!--                </a>-->
<!--            </header>-->
<!--            <p></p>-->
            <header class="js-cms-accordion-trigger my-account-nav-list__header backlink">
                <a href="<?=\yii\helpers\Url::toRoute('/account/logout')?>">Выйти</a>
            </header>
            <div class="js-cms-accordion-content my-account-nav-list__body">
                <ul class="my-account-nav-list__text"></ul>
            </div>
        </li>
    </ul>
</div>