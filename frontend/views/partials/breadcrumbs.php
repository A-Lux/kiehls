<ul class="breadcrumbs">
    <li class="Home breadcrumbs__item">
        <span itemscope="" >
            <a href="/" itemprop="url" title="Главная">
                <span itemprop="title">Главная</span>
            </a>
        </span>
        <span class="breadcrumbs__separator">/</span>
    </li>
    <? if($page == 'Account'):?>
        <li class="Invitations breadcrumbs__item last">
            <div class="item" itemscope="itemscope" >
                <span itemprop="title"><?=$page;?></span>
            </div>
        </li>
    <? else:?>
        <li class="Account breadcrumbs__item">
        <span itemscope="" >
            <a href="<?=\yii\helpers\Url::toRoute(['/account'])?>" itemprop="url" title="Account">
                <span itemprop="title">Account</span>
            </a>
        </span>
            <span class="breadcrumbs__separator">/</span>
        </li>

        <li class="Invitations breadcrumbs__item last">
            <div class="item" itemscope="itemscope">
                <span itemprop="title"><?=$page;?></span>
            </div>
        </li>

    <? endif;?>

</ul>